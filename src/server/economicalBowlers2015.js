function economicalBowlers(matches, deliveries) {

    const match2015 = matches.filter((val) => {
        if (val.season == 2015) {
            return val;
        }
    })


    let firstId = Number(match2015[0].id);
    let lastId = Number(match2015[match2015.length - 1].id);


    const filteredDeleveries = deliveries.filter((val) => {

        let num = Number(val.match_id);

        if (num >= firstId && num <= lastId) {
            return val;
        }
    })

    //return filteredDeleveries
    //516   576
//totalRuns

    let totalRuns = filteredDeleveries.reduce((result, each) => {

        if (result.hasOwnProperty(each.bowler)) {

            result[each.bowler] += Number(each.total_runs);

        }
        else {

            result[each.bowler] = Number(each.total_runs);

        }

        return result;

    }, {})
   

    //console.log(result1)
//totalBalls
    let totalBalls = filteredDeleveries.reduce((result, each) => {

        if (result.hasOwnProperty(each.bowler)) {
            if (Number(each.ball) == 6) {
                result[each.bowler] += 1;
            }

        }
        else {
            result[each.bowler] = 0;

        }
        return result;

    }, {})

    

    for (let property in totalRuns) {
        //(property, totalRuns[property], totalBalls[property])
        totalRuns[property] = totalRuns[property] / totalBalls[property];


    }

   

    const sortable = Object.fromEntries(
        Object.entries(totalRuns).sort(([, a], [, b]) => a - b)
    );

    const finalResult = Object.keys(sortable).slice(0, 10).reduce((result, key) => {

        result[key] = sortable[key];

        return result;
    }, {});
    return finalResult
}

module.exports = economicalBowlers
