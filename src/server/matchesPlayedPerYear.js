function matchesPlayedPerYear(matches) {
    const PlayedPer = matches.reduce((matchesPlayed, val) => {

        if (matchesPlayed[val.season]) {
            matchesPlayed[val.season] += 1;
        }
        else {
            matchesPlayed[val.season] = 1;

        }
        return matchesPlayed

    }, {});
    return PlayedPer;
    // console.log(matchesPlayedPerYear)
}

module.exports = matchesPlayedPerYear;