const fs = require("fs")
const csv = require('csvtojson')
const matchesPlayedPerYear = require("./matchesPlayedPerYear")
const matchesWonPerTeam = require("./matchesWonPerTeam")
const match2016 = require("./extraRun2016");
const economicalBowlers = require("./economicalBowlers2015")

const Matches_file_path = "../data/matches.csv";
const Matches_file_path1 = "../data/deliveries.csv";


const jsonFilePath = "../public/output/matchesPerYear.json"
const jsonFilePath1 = "../public/output/matchesWon.json"
const jsonFilePath2 = "../public/output/extraRun2016.json"
const jsonFilePath3 = "../public/output/economicalBowlers2015.json"


function main() {
    csv()
        .fromFile(Matches_file_path)

        .then(matches => {
            let result = matchesPlayedPerYear(matches);
            let result1 = matchesWonPerTeam(matches);

            saveMatchplayedYear(result)
            saveMatchplayedYear1(result1)
        });


    csv()
        .fromFile(Matches_file_path1)
        .then((deliveries) => {
            csv()
                .fromFile(Matches_file_path)
                .then((matches) => {
                    let result2 = (match2016(matches, deliveries))
                    let result3 = (economicalBowlers(matches, deliveries))

                    saveMatchplayedYear2(result2)
                    saveMatchplayedYear3(result3)


                })

        })

    // csv()
    // .fromFile(Matches_file_path1)
    // .then((deliveries) => {
    //     csv()
    //     .fromFile(Matches_file_path)
    //     .then((matches)=>{
    //         console.log(match2016(matches,deliveries))


    //     })

    // })




    function saveMatchplayedYear(result) {
        const jsonData = {
            matchesPlayedPerYear: result
        };
        const jsonString = JSON.stringify(jsonData)
        fs.writeFile(jsonFilePath, jsonString, "utf8", err => {
            if (err) {
                console.error(err)
            }
        });
    }

    function saveMatchplayedYear1(result1) {
        const jsonData = {
            matchesWonPerTeam: result1
        };
        const jsonString = JSON.stringify(jsonData)
        fs.writeFile(jsonFilePath1, jsonString, "utf8", err => {
            if (err) {
                console.error(err)
            }
        });
    }
    function saveMatchplayedYear2(result2) {
        const jsonData = {
            match2016: result2
        };
        const jsonString = JSON.stringify(jsonData)
        fs.writeFile(jsonFilePath2, jsonString, "utf8", err => {
            if (err) {
                console.error(err)
            }
        });
    }
    function saveMatchplayedYear3(result3) {
        const jsonData = {
            economicalBowlers: result3
        };
        const jsonString = JSON.stringify(jsonData)
        fs.writeFile(jsonFilePath3, jsonString, "utf8", err => {
            if (err) {
                console.error(err)
            }
        });
    }
}
main();