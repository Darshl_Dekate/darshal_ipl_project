function matchesWonPerTeam(matches) {
    const res = matches.reduce((matchesWon, eachData) => {


        if (matchesWon[eachData.winner]) {
            if (matchesWon[eachData.winner][eachData.season]) {
                matchesWon[eachData.winner][eachData.season] += 1
            }
            else {
                matchesWon[eachData.winner][eachData.season] = 1
            }
        }
        else {
            matchesWon[eachData.winner] = {};
            matchesWon[eachData.winner][eachData.season] = 1
        }

        return matchesWon;

    }, {})
    return res;
}
module.exports = matchesWonPerTeam;
//console.log(matchesWonPerTeam);
