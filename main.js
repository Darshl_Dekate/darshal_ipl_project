const fs = require("fs")
const csv = require('csvtojson')
const matchesPlayedPerYear = require("./src/server/matchesPlayedPerYear")
const Matches_file_path = "./src/data/matches.csv";
const jsonFilePath = "./src/public/output/matchesPerYear.json"
function main() {
    csv()
        .fromFile(Matches_file_path)
        .then(matches => {
            let result = matchesPlayedPerYear(matches);
            saveMatchplayedYear(result)
        });


    
}
function saveMatchplayedYear(result) {
    const jsonData = {
        matchesPlayedPerYear: result
    };
    const jsonString = JSON.stringify(jsonData)
    fs.writeFile(jsonFilePath, jsonString, "utf8", err => {
        if (err) {
            console.error(err)
        }
    });
}
main();